#ifndef ANGLECONVERTOR_H
#define ANGLECONVERTOR_H

#include "baseconvertor.h"

class AngleConvertor
        : public BaseConvertor
{
public:
    explicit AngleConvertor(QObject *parent = nullptr);
    double ToCentesimale(double from);
    double ToSexagesimal(double val);

protected:
    virtual void LoadRatio() override;
    virtual double Calculate(double from, double to, double value) override;

private:

};

#endif // ANGLECONVERTOR_H
