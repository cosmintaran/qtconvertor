#include "lengthconvertor.h"


LengthConvertor::LengthConvertor(QObject *parent)
    :BaseConvertor(parent)
{
    LoadRatio();
    FillList();
}

LengthConvertor::~LengthConvertor()
{

}

void LengthConvertor::LoadRatio()
{
    m_ratioMap["meters"]             = 1.0;
    m_ratioMap["feets"]              = 0.3048;
    m_ratioMap["inchs"]              = 0.0254;
    m_ratioMap["miles"]              = 1609.344;
    m_ratioMap["miles (nautical)"]   = 1852.0;
    m_ratioMap["yards"]              = 0.9144;
    m_ratioMap["kilometers"]         = 1000.0;
    m_ratioMap["hectometers"]        = 100.0;
    m_ratioMap["centimeters"]        = 0.01;
    m_ratioMap["milimeters"]         = 0.001;
    m_ratioMap["stanjen (austriac)"] = 1.89648;
}
