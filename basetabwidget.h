#ifndef TABAREAWIDGET_H
#define TABAREAWIDGET_H

#include <QWidget>
#include "baseconvertor.h"

namespace Ui {
class TabAreaWidget;
}

class BaseTabWidget : public QWidget
{
    Q_OBJECT

public:
    explicit BaseTabWidget(BaseConvertor* convertor, QWidget *parent = nullptr);
    ~BaseTabWidget();

private slots:
    void on_listView_clicked(const QModelIndex &index);
    void on_listView_2_clicked(const QModelIndex &index);

    void on_pushButton_clicked();

private:
    Ui::TabAreaWidget *ui;
    BaseConvertor* convertor;
};

#endif // TABAREAWIDGET_H
