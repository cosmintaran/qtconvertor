#include "basetabwidget.h"
#include "ui_basetabwidget.h"

BaseTabWidget::BaseTabWidget(BaseConvertor* convertor, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TabAreaWidget),convertor(convertor)
{
    ui->setupUi(this);
    ui->lineInput->setValidator(new QDoubleValidator(ui->lineInput));

    ui->listView->setModel(&convertor->Model());
    ui->listView_2->setModel(&convertor->Model());

    ui->listView->setCurrentIndex(convertor->Model().index(0,0));
    ui->lblInputSel->setText(convertor->Model().index(0,0).data().toString());

    ui->listView_2->setCurrentIndex(convertor->Model().index(0,0));
    ui->lblRezSel->setText(convertor->Model().index(0,0).data().toString());
    convertor->From(convertor->Model().index(0,0).data().toString());
    convertor->To(convertor->Model().index(0,0).data().toString());
}

BaseTabWidget::~BaseTabWidget()
{
    delete ui;
    delete convertor;
}

void BaseTabWidget::on_listView_clicked(const QModelIndex &index)
{
    ui->lblInputSel->clear();
    ui->lblInputSel->setText(index.data().toString());
    convertor->From(index.data().toString());

    double rez = convertor->Convert(ui->lineInput->text().toDouble());
    ui->lineRezult->setText(QString::number(rez));

}

void BaseTabWidget::on_listView_2_clicked(const QModelIndex &index)
{
    ui->lblRezSel->clear();
    ui->lblRezSel->setText(index.data().toString());
    convertor->To(index.data().toString());

    double rez = convertor->Convert(ui->lineInput->text().toDouble());
    ui->lineRezult->setText(QString::number(rez));
}

void BaseTabWidget::on_pushButton_clicked()
{
    double rez = convertor->Convert(ui->lineInput->text().toDouble());
    ui->lineRezult->setText(QString::number(rez));
}
