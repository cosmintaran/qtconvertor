#include "angleconvertor.h"
#include <QMessageBox>

AngleConvertor::AngleConvertor(QObject *parent)
    :BaseConvertor(parent)
{
    LoadRatio();
    FillList();
}

void AngleConvertor::LoadRatio()
{
    m_ratioMap["radians"] = 1.0;
    m_ratioMap["degres (centesimal)"] = 0.01745329251994329576923690768489;
    m_ratioMap["gons"] = 0.0157079632679489661923132169164;
    m_ratioMap["degres (sexagesimal)"] = 0.01745329251994329576923690768489;
}


double AngleConvertor::Calculate(double dfrom, double dto, double dvalue)
{
    if (From() == "degres (sexagesimal)"){
        double centValue = ToCentesimale(dvalue);
        if (centValue < 0)
            return 0;
        return BaseConvertor::Calculate(dfrom, dto, centValue);
    }
    if (To() == "degres (sexagesimal)"){

        double val = BaseConvertor::Calculate(dfrom, dto, dvalue);
        return ToSexagesimal(val);
    }

    return BaseConvertor::Calculate(dfrom, dto, dvalue);
}

double AngleConvertor::ToCentesimale(double value )
{
    int grades = (int)value;
    if (grades < 0 || grades > 359){
        QMessageBox::warning(nullptr, "Unit Converter", "Grades should have values betweens 0 - 359 ");
        return -1;
    }
    int minutes = (int)((value - grades)*100);

    if (minutes > 59){
        QMessageBox::warning(nullptr, "Unit Converter", "Minutes should have values betweens 0 - 59 ");
        return -1;
    }

    long double seconds = (((value - grades)*100 - minutes)*100);
    if (seconds >= 60){
        QMessageBox::warning(nullptr, "Unit Converter", "Seconds should have values betweens 0 - 59 ");
        return -1;
    }

    return (grades*3600 + minutes * 60 + seconds)/3600;
}

double AngleConvertor::ToSexagesimal(double val)
{
    int deg = (int)val;

    if (deg < 0 || deg > 359){
        QMessageBox::warning(nullptr, "Unit Converter", "Grades should have values betweens 0 - 359 ");
        return -1;
    }
    double mm = (val - deg) * 60;
    double ss = (mm - (int)mm) * 60;
    return deg + ((int)mm*0.01) + (ss * 0.0001);
}
