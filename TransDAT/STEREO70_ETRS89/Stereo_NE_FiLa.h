#ifndef STEREONEFILA_H
#define STEREONEFILA_H

class Stereo_NE_FiLa
{
public:
    Stereo_NE_FiLa(double ca, double cnf);
    ~Stereo_NE_FiLa();

    double GetFi(){ return _fi; }
    double GetLa(){ return _la; }
    void DoConversion(double North, double East);

private:
    double _a, _nf, _fi, _la;

};

#endif
