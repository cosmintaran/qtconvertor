#ifndef STEREO70HMNETRS88_H
#define STEREO70HMNETRS88_H

#include "../CommonClasses/TransDatResult.h"
class Stereo70HMN_ETRS89
{
public:
    Stereo70HMN_ETRS89();
    ~Stereo70HMN_ETRS89();
    TransDatResult DoTransformation(double north, double east, double z = 0);
    double GetFi(){ return _fi; }
    double GetLa(){ return _la; }
    double GetH() { return _h; }

private:
    double _fi, _la, _h;
};
#endif
