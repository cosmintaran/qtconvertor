#include "Stereo_NE_FiLa.h"
#include <qmath.h>


Stereo_NE_FiLa::Stereo_NE_FiLa(double ca, double cnf){
    _a = ca;
    _nf = cnf;
    _fi = 0;
    _la = 0;
}


Stereo_NE_FiLa::~Stereo_NE_FiLa(){
}

void Stereo_NE_FiLa::DoConversion(double North, double East)
{
    _fi = 0; _la = 0;
    double br1 = 46 * M_PI / 180;
    double la0 = 25 * M_PI / 180;
    double k0 = 0.99975;

    double f = 1 / _nf;
    double b = _a * (1 - f);
    double ep = sqrt((_a * _a - b * b) / (_a * _a));
    double w = sqrt(1 - ep * ep * sin(br1) * sin(br1));
    double radius = _a * (1 - ep * ep) / (w * w * w);
    radius = sqrt(radius * _a / w);
    double n = ep * ep * cos(br1) * cos(br1) * cos(br1) * cos(br1) / (1 - ep * ep);
    n = sqrt(1 + n);
    double s1 = (1 + sin(br1)) / (1 -sin(br1));
    double s2 = (1 - ep *sin(br1)) / (1 + ep * sin(br1));
    double w1 = exp(n * log(s1 * exp(ep * log(s2))));
    double c = (n + sin(br1)) * (1 - (w1 - 1) / (w1 + 1)) / ((n - sin(br1)) * (1 + (w1 - 1) / (w1 + 1)));
    double w2 = c * w1;

    double hi0 = (w2 - 1) / (w2 + 1);
    hi0 =atan(hi0 / sqrt(1 - hi0 * hi0));
    double g = 2 * radius * k0 * tan(M_PI / 4 - hi0 / 2);
    double h = 4 * radius * k0 * tan(hi0) + g;
    double fn = 500000;
    double fe = 500000;

    double ii =atan((East - fe) / (h + (North - fn)));
    double j = atan((East - fe) / (g - (North - fn))) - ii;
    double lam = j + 2 * ii + la0;
    _la = la0 + (lam - la0) / n;
    double hi = hi0 + 2 * atan((North - fn - (East - fe) * tan(j / 2)) / (2 * radius * k0));
    double csi = 0.5 * log((1 +sin(hi)) / (c * (1 - sin(hi)))) / n;
    _fi = 2 * atan(exp(csi)) - M_PI / 2;
    double i = 0;
    double tol = 0.000001;
    double dif = 100;
    while (dif > tol && i < 50)
    {
        i = i + 1;
        double fic = _fi;
        double csii = log(tan(_fi / 2 + M_PI / 4) * exp(ep / 2 *
           log((1 - ep * sin(_fi)) / (1 + ep * sin(_fi)))));
        _fi = _fi - (csii - csi) * cos(_fi) * (1 - ep * ep * sin(_fi) * sin(_fi)) / (1 - ep * ep);
        dif = abs(_fi * 180 * 60 * 60 / M_PI - fic * 180 * 60 * 60 / M_PI);
    }
}
