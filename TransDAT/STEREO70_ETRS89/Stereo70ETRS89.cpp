#include "Stereo70ETRS89.h"
#include "../CommonClasses/Interpolation_2D.h"
#include "../CommonClasses/GRD.h"
#include "../CommonClasses/Helmert2D.h"
#include "Stereo_NE_FiLa.h"

#include <qmath.h>

Stereo70ETRS89::Stereo70ETRS89(){
}


Stereo70ETRS89::~Stereo70ETRS89(){
}

TransDatResult Stereo70ETRS89::DoTransformation(double north, double east, double z /*= 0*/)
{
    TransDatResult error;
    Interpolation_2D int2D(north, east, GRD::GetNameFilegrd_R());
    error = int2D.Interpolate();
    if (error != TransDatResult::TD_OK) return error;

    Helmert2D H2D(GRD::tE_St70_OS, GRD::tN_St70_OS, GRD::dm_St70_OS, GRD::Rz_St70_OS);
    H2D.DoTransformation(east - int2D.GetShiftValue_E(), north - int2D.GetShiftValue_N());

    Stereo_NE_FiLa stNEFiLa(GRD::A_ETRS89, GRD::INV_F_ETRS89);
    stNEFiLa.DoConversion(H2D.GetNorthHelmert(), H2D.GetEastHelmert());
    //{=> (phi, la) in radians}
    //{Conversion radians to degrees}
    _fi = stNEFiLa.GetFi() * 180 / M_PI;
    _la = stNEFiLa.GetLa() * 180 / M_PI;

    return error;
}
