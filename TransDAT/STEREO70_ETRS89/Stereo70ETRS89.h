#ifndef STEREO70ETRS88_H
#define STEREO70ETRS88_H

#include "../CommonClasses/TransDatResult.h"
class Stereo70ETRS89
{
public:
    Stereo70ETRS89();
    ~Stereo70ETRS89();
    double GetFi(){ return _fi; }
    double GetLa(){ return _la; }
    TransDatResult DoTransformation(double north, double east, double z = 0);

private:
    double _fi;
    double _la;
};

#endif
