
#include "Stereo70HMN_ETRS89.h"
#include "Stereo70ETRS89.h"
#include "../CommonClasses/HMNETRS89.h"


Stereo70HMN_ETRS89::Stereo70HMN_ETRS89()
{
}


Stereo70HMN_ETRS89::~Stereo70HMN_ETRS89()
{
}

TransDatResult Stereo70HMN_ETRS89::DoTransformation(double north, double east, double z /*= 0*/)
{
    Stereo70ETRS89 stE;
    TransDatResult result = stE.DoTransformation(north,east);
    if (result != TransDatResult::TD_OK) return result;

    _fi = stE.GetFi();
    _la = stE.GetLa();
    HMNETRS89 hMNE;
    result = hMNE.DoTransformation(stE.GetFi(), stE.GetLa(), z);
    if (result != TransDatResult::TD_OK) return result;
    _h = hMNE.GetHeight();

    return result;
}
