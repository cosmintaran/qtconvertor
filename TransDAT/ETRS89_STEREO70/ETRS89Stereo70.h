#ifndef ETRS89_STEREO_H
#define ETRS89_STEREO_H

#include "ETRS89_STEREO.h"

class ETRS89Stereo70 :
    public ETRS89_STEREO
{
public:
    ETRS89Stereo70();
    ~ETRS89Stereo70();

    virtual TransDatResult DoTransformation(double phi, double la, double h = 0) override;

};

#endif
