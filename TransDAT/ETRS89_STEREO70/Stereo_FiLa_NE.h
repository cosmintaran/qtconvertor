#ifndef STEREOFILANE_H
#define STEREOFILANE_H

class Stereo_FiLa_NE
{
public:
    Stereo_FiLa_NE(double ca, double cnf);
    ~Stereo_FiLa_NE();
    void DoConversion(double Fi, double La);
    double GetNorth(){ return m_north; }
    double GetEast(){ return m_east; }

private:
    double m_a, m_cnf;
    double m_north;
    double m_east;
};

#endif
