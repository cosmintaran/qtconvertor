#include "ETRS89Stereo30.h"
#include "../CommonClasses/StereoNE.h"
#include "../CommonClasses/Helmert2D.h"
#include "../CommonClasses/GRD.h"


ETRS89Stereo30::ETRS89Stereo30(){
}


ETRS89Stereo30::~ETRS89Stereo30(){
}

TransDatResult ETRS89Stereo30::DoTransformation(double phi, double la, double h /*= 0*/)
{
    StereoNE st = GetStereoNE(phi, la, h);

    Helmert2D H2D(GRD::tE_OS_St30, GRD::tN_OS_St30, GRD::dm_OS_St30, GRD::Rz_OS_St30);
    H2D.DoTransformation(st.GetEast(), st.GetNorth());
    Interpolation(GRD::GetNameFilegrd_B(), H2D.GetNorthHelmert(), H2D.GetEastHelmert());

    return TransDatResult::TD_OK;
}
