#ifndef ETRS89STEREO70HMN_H
#define ETRS89STEREO70HMN_H

#include "../CommonClasses/TransDatResult.h"
class ETRS89Stereo70HMN
{
public:

    ETRS89Stereo70HMN();
    ~ETRS89Stereo70HMN();
    TransDatResult DoTransformation(double phi, double la, double h);
    double GetNorth(){ return _north; }
    double GetEast() { return _east; }
    double GetHMn()  { return _hMn; }

private:

    double _north;
    double _east;
    double _hMn;
};

#endif
