#include "ETRS89Stereo70HMN.h"
#include "ETRS89Stereo70.h"
#include "ETRS89HMN.h"

ETRS89Stereo70HMN::ETRS89Stereo70HMN(){

}


ETRS89Stereo70HMN::~ETRS89Stereo70HMN(){

}

TransDatResult ETRS89Stereo70HMN::DoTransformation(double phi, double la, double h)
{
    ETRS89HMN eMHN;
    TransDatResult Result = eMHN.DoTransformation(phi, la, h);
    if (Result != TransDatResult::TD_OK) return Result;

    _hMn = eMHN.GetHeight();

    ETRS89Stereo70 etrsSt70;
    Result = etrsSt70.DoTransformation(phi, la, h);
    if (Result != TransDatResult::TD_OK) return Result;
    _east = etrsSt70.GetEast();
    _north = etrsSt70.GetNorth();
    return Result;
}
