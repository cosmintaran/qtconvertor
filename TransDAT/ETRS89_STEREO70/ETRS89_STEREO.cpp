#include "ETRS89_STEREO.h"
#include "../CommonClasses/GRD.h"
#include "Stereo_FiLa_NE.h"
#include "../CommonClasses/Interpolation_2D.h"

#include <qmath.h>

StereoNE ETRS89_STEREO::GetStereoNE(double phi, double la, double h = 0){

    //{Conversion degrees to radians}
    double lPhi = phi * M_PI / 180;
    double lLa = la * M_PI / 180;

    //{Conversion GRS80 to Oblique Stereographic}
    Stereo_FiLa_NE stFiLaNE (GRD::A_ETRS89, GRD::INV_F_ETRS89);
    stFiLaNE.DoConversion(lPhi, lLa);
    return StereoNE(stFiLaNE.GetNorth(), stFiLaNE.GetEast());

}

void ETRS89_STEREO::Interpolation(QString nameGridn, double north, double east){

    Interpolation_2D int2D (north,east, nameGridn);
    TransDatResult result = int2D.Interpolate();
    if (result == TransDatResult::TD_OK)
    {
        _east = east + int2D.GetShiftValue_E();
        _north = north + int2D.GetShiftValue_N();
    }

}

