#include "ETRS89HMN.h"
#include "../CommonClasses/Interpolation_1D.h"
#include "../CommonClasses/GRD.h"


ETRS89HMN::ETRS89HMN() { }


ETRS89HMN::~ETRS89HMN(){ }

TransDatResult ETRS89HMN::DoTransformation(double phi, double la, double h)
{
    Interpolation_1D int1D(phi, la, GRD::GetNameFilegrdz());
    auto error = int1D.Interpolate();
    if (error != TransDatResult::TD_OK) return error;

    double shift_value = int1D.GetShiftValueH();
    _height = h - shift_value;
    return error;
}
