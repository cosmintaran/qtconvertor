
#include "Stereo_FiLa_NE.h"
#include <qmath.h>

Stereo_FiLa_NE::Stereo_FiLa_NE(double ca, double cnf)
{
    m_a = ca;
    m_cnf = cnf;
}


Stereo_FiLa_NE::~Stereo_FiLa_NE(){
}

void Stereo_FiLa_NE::DoConversion(double Fi, double La)
{
    double br1 = 46 * M_PI / 180;
    double la0 = 25 * M_PI / 180;
    double k0 = 0.99975;
    double f = 1 / m_cnf;
    double b = m_a * (1 - f);
    double square_m_a = m_a * m_a;
    double ep = sqrt((square_m_a - b * b) / square_m_a);
    double w = sqrt(1 - ep * ep * (sin(br1)) * (sin(br1)));
    double radius = (m_a * (1 - ep * ep)) / (w * w * w);
    radius = sqrt(radius * m_a / w);
    double n = sqrt(1 + (ep * ep * cos(br1) * cos(br1) * cos(br1) * cos(br1)) / (1 - ep * ep));
    double s1 = (1 + sin(br1)) / (1 - sin(br1));
    double s2 = (1 - ep * sin(br1)) / (1 + ep * sin(br1));
    double w1 = exp(n * log(s1 * exp(ep * log(s2))));
    double c = ((n + sin(br1)) * (1 - (w1 - 1) / (w1 + 1))) / ((n - sin(br1)) * (1 + (w1 - 1) / (w1 + 1)));
    double w2 = c * w1;
    double hi0 = (w2 - 1) / (w2 + 1);
    hi0 = atan(hi0 / sqrt(1 - hi0 * hi0));
    double sa = (1 + sin(Fi)) / (1 - sin(Fi));
    double sb = (1 - ep * sin(Fi)) / (1 + ep * sin(Fi));
    w = c * exp(n * log(sa * exp(ep * log(sb))));
    double hi = (w - 1) / (w + 1);
    hi = atan(hi / sqrt(1 - hi * hi));
    double lam = n * (La - la0) + la0;
    double beta = 1 + sin(hi) * sin(hi0) + cos(hi) * cos(hi0) * cos(lam - la0);
    m_east = 2 * radius * k0 * cos(hi) * sin(lam - la0) / beta;
    m_north = 2 * radius * k0 * (cos(hi0) * sin(hi) - sin(hi0) * cos(hi) * cos(lam - la0)) / beta;
    m_north = m_north + 500000;
    m_east = m_east + 500000;
}
