#ifndef ETRS89HMN_H
#define ETRS89HMN_H

#include "../CommonClasses/TransDatResult.h"

class ETRS89HMN
{
public:
    ETRS89HMN();
    ~ETRS89HMN();
    TransDatResult DoTransformation(double phi, double la, double h);
    double GetHeight(){ return _height; }

private:
    double _height;
};

#endif
