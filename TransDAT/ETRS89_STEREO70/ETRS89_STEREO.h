#ifndef ETRS89STEREO_H
#define ETRS89STEREO_H

#include "../CommonClasses/StereoNE.h"
#include "../CommonClasses/TransDatResult.h"
#include <QString>

class ETRS89_STEREO
{
public:
    ETRS89_STEREO() { }
    ~ETRS89_STEREO(){ }
    virtual TransDatResult DoTransformation(double phi, double la, double h = 0) { return TransDatResult::TD_OK; }
    double GetNorth(){ return _north; }
    double GetEast(){ return _east; }

protected:
    StereoNE GetStereoNE(double phi, double la, double h /*= 0*/);
    void Interpolation(QString nameGridn, double north, double east);
    double _north, _east;
};

#endif
