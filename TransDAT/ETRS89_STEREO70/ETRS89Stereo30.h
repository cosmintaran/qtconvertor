#ifndef ETRS89STEREO30_H
#define ETRS89STEREO30_H

#include "ETRS89_STEREO.h"

class ETRS89Stereo30 :
    public ETRS89_STEREO
{
public:
    ETRS89Stereo30();
    ~ETRS89Stereo30();

    virtual TransDatResult DoTransformation(double phi, double la, double h = 0) override;


};

#endif
