#include "ETRS89Stereo70.h"
#include "../CommonClasses/StereoNE.h"
#include "../CommonClasses/Helmert2D.h"
#include "../CommonClasses/GRD.h"


ETRS89Stereo70::ETRS89Stereo70(){
}


ETRS89Stereo70::~ETRS89Stereo70(){
}

TransDatResult ETRS89Stereo70::DoTransformation(double phi, double la, double h /*= 0*/)
{
    StereoNE st = GetStereoNE(phi, la, h);

    Helmert2D H2D (GRD::tE_OS_St70, GRD::tN_OS_St70, GRD::dm_OS_St70, GRD::Rz_OS_St70);
    H2D.DoTransformation(st.GetEast(), st.GetNorth());
    Interpolation(GRD::GetNameFilegrd_R(), H2D.GetNorthHelmert(), H2D.GetEastHelmert());

    return TransDatResult::TD_OK;
}
