#ifndef GRD_H
#define GRD_H

#include <QString>

struct GRD {

private:
        static const QString _nameFilegrdz;
        static const QString _nameFilegrd_R;
        static const QString _nameFilegrd_B;
        static QString _gridFilePath;

public:
    static void SetGridFilesPath(const QString& gridFilePath) { _gridFilePath = gridFilePath; }
    static QString GetGridFilesPath(){ return _gridFilePath; }
    static QString GetNameFilegrdz() {
         QString ss = QString("%1/%2").arg(_gridFilePath,_nameFilegrdz);
        return ss;
    }
    static QString GetNameFilegrd_R(){
        QString ss = QString("%1/%2").arg(_gridFilePath,_nameFilegrd_R);
        return ss;
    }

    static QString GetNameFilegrd_B(){
         QString ss = QString("%1/%2").arg(_gridFilePath,_nameFilegrd_B);
        return ss;
    }

    /*
    *     Grd files are Delphi binary files with the types:
    *    - double for EGG97_QGRJ.GRD
    *    - record for ETRS89_KRASOVSCHI42_2DJ.GRD and ETRS89_Stereo30_2DJ.GRD with
    *      a declaration like this:
    *        tnod=record
    *          dE, dN:real;
    *        end;
    *      Note: In Delphi, real=double.
    *    Each of the grid files has a header (minE, maxE, minN, maxN, stepE, stepN),
    *    and then the data records are written consecutively in rows from lower left
    *    corner to the upper right corner starting with position 6 (positions 0..5
    *    are for header):
    *                size_N . . . . . . .  . .
    *                   .                    .
    *                   .                    .
    *                   6, 7, 8, . . . . . size_E
    *    For 2D grid files, the order is: dE1, dN1, dE2, dN2, etc.
    */

    //* Parameters of ellipsoid GRS80 *
    static const double A_ETRS89;
    static const double INV_F_ETRS89;

    //* Helmert 2D Transformation parameters:

    //* Oblique_Stereographic_GRS80 to Stereografic 1970 *
    static const double tE_OS_St70; //{translation in direction of East in meters}
    static const double tN_OS_St70;  //{translation in direction of North in meters}
    static const double dm_OS_St70; //{scale parameter in ppm (scale m = 1+dm*1e-6)}
    static const double Rz_OS_St70; //{rotation around z axis in seconds ('')}

    //* Stereografic 1970 to Oblique_Stereographic_GRS80 *
    static const double tE_St70_OS; //{translation in direction of East in meters}
    static const double tN_St70_OS;  //{translation in direction of North in meters}
    static const double dm_St70_OS; //{scale parameter in ppm (scale m = 1+dm*1e-6)}
    static const double Rz_St70_OS; //{rotation around z axis in seconds ('')}

    //* Oblique_Stereographic_GRS80 to Stereografic 1930 *
    static const double tE_OS_St30; //{translation in direction of East in meters}
    static const double tN_OS_St30;  //{translation in direction of North in meters}
    static const double dm_OS_St30; //{scale parameter in ppm (scale m = 1+dm*1e-6)}
    static const double Rz_OS_St30; //{rotation around z axis in seconds ('')}

    //* Stereografic 1930 to Oblique_Stereographic_GRS80 *
    static const double tE_St30_OS; //{translation in direction of East in meters}
    static const double tN_St30_OS;  //{translation in direction of North in meters}
    static const double dm_St30_OS; //{scale parameter in ppm (scale m = 1+dm*1e-6)}
    static const double Rz_St30_OS; //{rotation around z axis in seconds ('')} 
};

#endif //GRD_H
