#ifndef BSPLINEINTERPOLATION_H
#define BSPLINEINTERPOLATION_H

class BSplineInterpolation
{
public:
    BSplineInterpolation(){ _shiftValue = 0; }
    ~BSplineInterpolation(){}
    void DoBSInterpolation(double ff[], double az[]);
    double GetShiftValue();

private:
    double _shiftValue;
    BSplineInterpolation(BSplineInterpolation &bs);
    BSplineInterpolation operator=(BSplineInterpolation &bs);
};
#endif //BSPLINEINTERPOLATION_H
