#ifndef POSITION_H
#define POSITION_H

struct Position{

    double GetNorth()const{ return _north; }
    double GetEast()const{ return _east; }
    double GetElevation()const{ return _elev; }

    void SetNorth(double value){_north = value; }
    void SetEast(double value){ _east = value; }
    void SetElevation(double value){ _elev = value; }

    Position(double north, double east, double elev){
        _north = north;
        _east = east;
        _elev = elev;
    }
    Position() = delete;
    Position(const Position &old){
        _north = old._north;
        _east = old._east;
        _elev = old._elev;
    }
    void operator=(const Position &old){
        _north = old._north;
        _east = old._east;
        _elev = old._elev;
    }

private:
    double _north;
    double _east;
    double _elev;
};

#endif
