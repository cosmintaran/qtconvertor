#include "Helmert2D.h"
#include <qmath.h>

void Helmert2D::DoTransformation(double EastS, double NorthS)
{
    double m = 1 + _dm * 1e-6;
    //{Conversion '' to radians}
    _rz = _rz * M_PI / (180 * 3600);
    _estHelmert = EastS * m * cos(_rz) - NorthS * m * sin(_rz) + _tE;
    _northHelmert = NorthS * m * cos(_rz) + EastS * m * sin(_rz) + _tN;
}
