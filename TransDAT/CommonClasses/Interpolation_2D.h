#ifndef INTERPOLATION2D_H
#define INTERPOLATION2D_H

#include "Interpolation_1D.h"

class Interpolation_2D :
    public Interpolation_1D
{
public:
    Interpolation_2D(double cphi, double cla, QString grdFileName);
    ~Interpolation_2D();
    TransDatResult Interpolate()override;
    double GetShiftValue_E(){ return _shiftValue_E; }
    double GetShiftValue_N(){ return _shiftValue_N; }

private:
    double _shiftValue_E, _shiftValue_N;
};

#endif
