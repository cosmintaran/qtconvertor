#include "HMNETRS89.h"
#include "Interpolation_1D.h"
#include "GRD.h"


HMNETRS89::~HMNETRS89(){

}

TransDatResult HMNETRS89::DoTransformation(double phi, double la, double h)
{
    Interpolation_1D int1D(phi, la, GRD::GetNameFilegrdz());
    TransDatResult error = int1D.Interpolate();
    if (error != TransDatResult::TD_OK) return error;

    double shift_value = int1D.GetShiftValueH();
    _height = h + shift_value;
    return error;
}
