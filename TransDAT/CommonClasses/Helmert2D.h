#ifndef HELMERT2D_H
#define HELMERT2D_H

class Helmert2D
{
public:
    Helmert2D(double ctE, double ctN, double cdm, double cRz)
        :_tE(ctE), _tN(ctN), _dm(cdm), _rz(cRz)
    {

    }
    ~Helmert2D() { }

    double GetNorthHelmert(){ return _northHelmert; }
    double GetEastHelmert(){ return _estHelmert; }
    void DoTransformation(double EastS, double NorthS);

private:
    double _tE, _tN, _dm, _rz, _estHelmert, _northHelmert;
};

#endif
