#include "GRD.h"

const QString GRD::_nameFilegrdz = "EGG97_QGRJ.GRD";
const QString GRD::_nameFilegrd_R = "ETRS89_KRASOVSCHI42_2DJ.GRD";
const QString GRD::_nameFilegrd_B = "ETRS89_Stereo30_2DJ.GRD";
QString GRD::_gridFilePath = "";

const double GRD::A_ETRS89 = 6378137;
const double GRD::INV_F_ETRS89 = 298.257222101;

const double GRD::tE_OS_St70 = 119.7358;
const double GRD::tN_OS_St70 = 31.8051;
const double GRD::dm_OS_St70 = 0.11559991;
const double GRD::Rz_OS_St70 = -0.22739706;

const double GRD::tE_St70_OS = -119.7358;
const double GRD::tN_St70_OS = -31.8051;
const double GRD::dm_St70_OS = -0.11559991;
const double GRD::Rz_St70_OS = 0.22739706;

const double GRD::tE_OS_St30 = -32701.3610;
const double GRD::tN_OS_St30 = 13962.1632;
const double GRD::dm_OS_St30 = 13.97707176;
const double GRD::Rz_OS_St30 = -1006.26886396;

const double GRD::tE_St30_OS = 32768.6284;
const double GRD::tN_St30_OS = -13802.2702;
const double GRD::dm_St30_OS = -13.97689927;
const double GRD::Rz_St30_OS = 1006.26886393;

