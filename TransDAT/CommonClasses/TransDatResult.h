#ifndef TRANSDATRESULT_H
#define TRANSDATRESULT_H

enum class TransDatResult{
    TD_OK = 0,
    TD_INVALID_GRID_FILES_PATH = 1,
    TD_NO_SUCH_DIRECTORY = 2,
    TD_GRID_FILE_MISSING =  3,
    TD_OUTSIDE_BORDER =  4,
    TD_UNDEFINED = 5
};

#endif
