#ifndef STEREONE_H
#define STEREONE_H

struct StereoNE{

    double GetNorth(){ return _north; }
    double GetEast(){ return _east; }
    StereoNE(double north, double east){
        _north = north;
        _east = east;
    }
    StereoNE() = delete;
    StereoNE(const StereoNE &old){
        _north = old._north;
        _east =old._east;
    }
    void operator=(const StereoNE &old){
        _north = old._north;
        _east = old._east;
    }

private:
    double _north;
    double _east;
};

#endif
