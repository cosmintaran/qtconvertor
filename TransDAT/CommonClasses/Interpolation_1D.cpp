
#include "Interpolation_1D.h"
#include <fstream>
#include "BSplineInterpolation.h"
#include <utility>


Interpolation_1D::Interpolation_1D(double cphi, double cla, QString grdFileName){
    _phi = cphi;
    _la = cla;
    _grdFileName = std::move(grdFileName);
    memset(nc, 0,sizeof(double) * 17);
    memset(ff, 0,sizeof(double) * 17);
}

Interpolation_1D::~Interpolation_1D(){   }

TransDatResult Interpolation_1D::Interpolate()
{
    TransDatResult result;
    double az[17];
    memset(az, 0, sizeof(double) * 17);
    int outside = 0;
    QFile file(_grdFileName);

    if (!file.open(QIODevice::ReadOnly)){
        return TransDatResult::TD_GRID_FILE_MISSING;
    }

    result = LoadArrays(file);

    if (result == TransDatResult::TD_OK)
    {
        for (int i = 1; i <= 16; ++i)
        {
            int pos = nc[i] * 8 - 8 + 48;
            file.seek(pos);
            double value = ReadFp(file);
            if ((int)qRound(value) == 999)
            {
                outside = -1;
                break;
            }
            az[i] = value;
        }
    }

    file.close();

    if (outside != 0 || TransDatResult::TD_OK != result)
        return TransDatResult::TD_OUTSIDE_BORDER;

    BSplineInterpolation bsi;
    bsi.DoBSInterpolation(ff, az);
    _shiftValueH = bsi.GetShiftValue();

    return result;
}

TransDatResult Interpolation_1D::LoadArrays(QFile &file)
{
    double minE = ReadFp(file);
    double maxE = ReadFp(file);
    double minN = ReadFp(file);
    double maxN = ReadFp(file);
    double stepE = ReadFp(file);
    double stepN = ReadFp(file);

    long nrjx = (long)qRound((maxE - minE) / stepE + 1);
    if (_la <= minE + stepE || _la >= maxE - stepE ||
        _phi <= minN + stepN || _phi >= maxN - stepN)
    {
        return TransDatResult::TD_OUTSIDE_BORDER;
    }

    long nodcolx = (long)abs(qRound(((_la - minE) / stepE)));
    long nodliny = (long)abs(qRound(((_phi - minN) / stepN)));
    double xk = minE + nodcolx * stepE;
    double yk = minN + nodliny * stepN;

    //{relative coordinate of point X:}
    xk = (_la - xk) / stepE;
    yk = (_phi - yk) / stepN;

    //{Parameters of bicubic spline surface}
    ff[0] = 0;
    ff[1] = 1;
    ff[2] = xk;
    ff[3] = xk * xk;
    ff[4] = xk * xk * xk;
    ff[5] = yk;
    ff[6] = xk * yk;
    ff[7] = xk * xk * yk;
    ff[8] = xk * xk * xk * yk;
    ff[9] = yk * yk;
    ff[10] = xk * yk * yk;
    ff[11] = xk * xk * yk * yk;
    ff[12] = xk * xk * xk * yk * yk;
    ff[13] = yk * yk * yk;
    ff[14] = xk * yk * yk * yk;
    ff[15] = xk * xk * yk * yk * yk;
    ff[16] = xk * xk * xk * yk * yk * yk;

    //{Positions in grid file}
    nc[6] = nodliny * nrjx + nodcolx + 1;
    nc[1] = (nodliny - 1) * nrjx + nodcolx;
    nc[2] = (nodliny - 1) * nrjx + nodcolx + 1;
    nc[3] = (nodliny - 1) * nrjx + nodcolx + 2;
    nc[4] = (nodliny - 1) * nrjx + nodcolx + 3;
    nc[5] = nodliny * nrjx + nodcolx;
    nc[7] = nodliny * nrjx + nodcolx + 2;
    nc[8] = nodliny * nrjx + nodcolx + 3;
    nc[9] = (nodliny + 1) * nrjx + nodcolx;
    nc[10] = (nodliny + 1) * nrjx + nodcolx + 1;
    nc[11] = (nodliny + 1) * nrjx + nodcolx + 2;
    nc[12] = (nodliny + 1) * nrjx + nodcolx + 3;
    nc[13] = (nodliny + 2) * nrjx + nodcolx;
    nc[14] = (nodliny + 2) * nrjx + nodcolx + 1;
    nc[15] = (nodliny + 2) * nrjx + nodcolx + 2;
    nc[16] = (nodliny + 2) * nrjx + nodcolx + 3;

    return TransDatResult::TD_OK;
}

double Interpolation_1D::ReadFp(QFile & file) const
{
    char ba[8];
    file.read(ba, 8);
    return *(double*)(ba);
}
