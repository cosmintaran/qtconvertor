#include "Interpolation_2D.h"
#include <fstream>
#include "BSplineInterpolation.h"


Interpolation_2D::Interpolation_2D(double cphi, double cla, QString grdFileName):
Interpolation_1D(cphi, cla, grdFileName) {
}


Interpolation_2D::~Interpolation_2D() {
}

TransDatResult Interpolation_2D::Interpolate() {

    TransDatResult result;
    double ax[17];
    double ay[17];
    memset(ax, 0, sizeof(double) * 17);
    memset(ay, 0, sizeof(double) * 17);
    int outside = 0;
    QFile file(_grdFileName);


    if (! file.open(QIODevice::ReadOnly)){
        return TransDatResult::TD_GRID_FILE_MISSING;
    }
    result = LoadArrays(file);
        if (result != TransDatResult::TD_OK) return result;
        for (auto i = 1; i <= 16; ++i)
        {
            file.seek(nc[i] * 16 - 16 + 48);
            double value = ReadFp(file);
            if ((int)qRound(value) == 999){
                outside = -1;
            }
            ax[i] = value;
            file.seek(nc[i] * 16 - 8 + 48);
            value = ReadFp(file);
            if ((int)qRound(value) == 999){
                outside = -1;
            }
            ay[i] = value;
        }
    
        file.close();

    if (outside != 0)
        return TransDatResult::TD_OUTSIDE_BORDER;


    BSplineInterpolation bsi ;
    bsi.DoBSInterpolation(ff, ax);
    _shiftValue_E = bsi.GetShiftValue();
    bsi.DoBSInterpolation(ff, ay);
    _shiftValue_N = bsi.GetShiftValue();

    return result;
}
