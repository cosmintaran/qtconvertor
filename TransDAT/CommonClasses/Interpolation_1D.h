#ifndef INTERPOLATION1D_H
#define INTERPOLATION1D_H

#include "TransDatResult.h"
#include <QString>
#include <QFile>

class Interpolation_1D
{
public:
    Interpolation_1D(double cphi, double cla, QString grdFileName);
    virtual ~Interpolation_1D();
    virtual TransDatResult Interpolate();
    double GetShiftValueH(){ return _shiftValueH; }

protected:    
    long nc[17];
    double ff[17];
    QString _grdFileName;
    double ReadFp(QFile & file) const;
    TransDatResult LoadArrays(QFile &file);

private:
    double _phi, _la, _shiftValueH;
};

#endif
