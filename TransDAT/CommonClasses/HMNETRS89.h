#ifndef HMNETRS89_H
#define HMNETRS89_H

#include "TransDatResult.h"

class HMNETRS89
{
public:
    HMNETRS89(){ _height = 0.0f; }
    ~HMNETRS89();
    TransDatResult DoTransformation(double phi, double la, double h);
    double GetHeight(){ return _height; }

private:
    double _height;
};

#endif
