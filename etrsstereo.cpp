#include "etrsstereo.h"
#include "ui_etrsstereo.h"
#include "angleconvertor.h"
#include "TransDAT/CommonClasses/GRD.h"

#include <QString>
#include <QDir>
#include <sstream>
#include <QMessageBox>

EtrsStereo::EtrsStereo(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::EtrsStereo)
{
    ui->setupUi(this);
    ui->lineEdit_B->setValidator(new QDoubleValidator());
    ui->lineEdit_L->setValidator(new QDoubleValidator());
    ui->lineEdit_H->setValidator(new QDoubleValidator());
    ui->comboBox->addItem("Degrees (sexagesimal)");
    ui->comboBox->addItem("Degrees (centesimal)");
}

EtrsStereo::~EtrsStereo()
{
    delete ui;
}

void EtrsStereo::on_pushButton_clicked()
{
    double latitude = ui->lineEdit_B->text().toDouble();
    double longitude = ui->lineEdit_L->text().toDouble();
    double height = ui->lineEdit_H->text().toDouble();

    QString m_selectedInputAngleType = ui->comboBox->itemText(ui->comboBox->currentIndex());

    if (m_selectedInputAngleType == "Degrees (sexagesimal)"){
        AngleConvertor angConv;
        latitude = angConv.ToCentesimale(latitude);
        if (latitude < 0) return;
        longitude = angConv.ToCentesimale(longitude);
        if (longitude < 0) return;
    }

    GRD::SetGridFilesPath(QDir::currentPath() + "/GRD_Files");
    ETRS89Stereo70HMN stereo70;
    TransDatResult rez = stereo70.DoTransformation(latitude, longitude, height);

    if (rez == TransDatResult::TD_OK){
        ui->lineEdit_X->setText(QString::number(stereo70.GetNorth() ,'f',3));
        ui->lineEdit_Y->setText(QString::number(stereo70.GetEast()  ,'f',3));
        ui->lineEdit_Z->setText(QString::number(stereo70.GetHMn()   ,'f',3));
    }
    else{
        DisplayError(rez, QDir::currentPath() + "/GRD_Files");
    }
}

void EtrsStereo::DisplayError(TransDatResult rez, QString path)
    {
        QString ss;
        switch (rez)
        {
        case TransDatResult::TD_INVALID_GRID_FILES_PATH:
        case TransDatResult::TD_NO_SUCH_DIRECTORY:
            ss.append("Invalid path: \"");
            ss.append(path);
            ss.append("\"");
            break;
        case TransDatResult::TD_GRID_FILE_MISSING:
            ss.append("The .GRD files are missing");
            break;
        case TransDatResult::TD_OUTSIDE_BORDER:
            ss.append("The added coordinates are outside border. Wrong coordinates");
            break;
        case TransDatResult::TD_UNDEFINED:
            ss.append("Ups. Something went terrible wrong.");
            break;
        default:
            break;
        }
        QMessageBox::warning(this,"ETRS89->STEREO70",ss, QMessageBox::Ok);
        //QtWarningMsg(ss.str().c_str(), _T("Error"), MB_OK | MB_ICONERROR);
    }
