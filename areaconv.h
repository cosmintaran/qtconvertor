#ifndef AREACONV_H
#define AREACONV_H

#include "baseconvertor.h"

class AreaConv
        : public BaseConvertor
{
public:
    explicit AreaConv(QObject *parent = nullptr);
    ~AreaConv();


protected:
    virtual void LoadRatio() override;
};

#endif // AREACONV_H
