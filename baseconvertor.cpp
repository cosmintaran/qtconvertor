#include "baseconvertor.h"

BaseConvertor::BaseConvertor(QObject *parent)
    : QObject(parent)
{
    model = new QStringListModel(this);
}

void BaseConvertor::FillList()
{
    QStringList list;
    for(auto& e : m_ratioMap){
        list << e.first;
    }

    model->setStringList(list);
}
