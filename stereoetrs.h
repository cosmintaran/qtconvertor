#ifndef STEREOETRS_H
#define STEREOETRS_H

#include <QWidget>

namespace Ui {
class StereoEtrs;
}

class StereoEtrs : public QWidget
{
    Q_OBJECT

public:
    explicit StereoEtrs(QWidget *parent = nullptr);
    ~StereoEtrs();

private:
    Ui::StereoEtrs *ui;
};

#endif // STEREOETRS_H
