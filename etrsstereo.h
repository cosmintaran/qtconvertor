#ifndef ETRSSTEREO_H
#define ETRSSTEREO_H

#include <QWidget>
#include "TransDAT/ETRS89_STEREO70/ETRS89Stereo70HMN.h"
#include "TransDAT/CommonClasses/GRD.h"

namespace Ui {
class EtrsStereo;
}

class EtrsStereo : public QWidget
{
    Q_OBJECT

public:
    explicit EtrsStereo(QWidget *parent = nullptr);
    ~EtrsStereo();

private slots:
    void on_pushButton_clicked();

private:
    Ui::EtrsStereo *ui;
    void DisplayError(TransDatResult rez, QString path);
};

#endif // ETRSSTEREO_H
