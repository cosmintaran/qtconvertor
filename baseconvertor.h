#ifndef BASECONVERTOR_H
#define BASECONVERTOR_H

#include <QObject>
#include <QString>
#include <QMap>
#include <QStringListModel>

class BaseConvertor : public QObject
{
    Q_OBJECT
public:

    QString From() const { return from; }
    void From(QString val) { from = val; }

    QString To() const { return to; }
    void To(QString val) { to = val; }

    QStringListModel& Model(){return *model;}

    std::map<QString, double>& GetSuportedConversion() { return m_ratioMap; };

     double Convert(double value){

        if (from == to || from.isEmpty() || to.isEmpty()) return value;
        auto itFrom = m_ratioMap.find(from);
        auto itTo = m_ratioMap.find(to);

        if (itFrom == m_ratioMap.end() && itTo == m_ratioMap.end())
            return value;

        return Calculate(itFrom->second, itTo->second, value);

    }

protected:
     explicit BaseConvertor(QObject *parent = nullptr);

     virtual double Calculate(double from, double to, double value){
         long double asMeter = value * from;
         return asMeter *(1 / to);
     }

     std::map<QString, double> m_ratioMap;
     virtual void LoadRatio() = 0;
     virtual void FillList();

     QStringListModel* model;


signals:

private:
    QString from, to;

    BaseConvertor(const BaseConvertor&);
    BaseConvertor& operator=(const BaseConvertor&);

};

#endif // BASECONVERTOR_H
