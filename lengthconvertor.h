#ifndef LENGTHCONVERTOR_H
#define LENGTHCONVERTOR_H

#include "baseconvertor.h"

class LengthConvertor
        : public BaseConvertor
{
public:
    explicit LengthConvertor(QObject *parent = nullptr);
    ~LengthConvertor();

protected:
    virtual void LoadRatio() override;
};

#endif // LENGTHCONVERTOR_H
