#include "areaconv.h"

AreaConv::AreaConv(QObject *parent)
    :BaseConvertor(parent)
{
    LoadRatio();
    FillList();
}

AreaConv::~AreaConv()
{

}

void AreaConv::LoadRatio()
{
     m_ratioMap["square meters"] = 1.0;
     m_ratioMap["square feets"] = 0.09290304;
     m_ratioMap["square inchs"] = 0.00064516;
     m_ratioMap["square miles"] = 2589988.110336;
     m_ratioMap["square yards"] = 0.83612736;
     m_ratioMap["square kilometers"] = 1000000.0;
     m_ratioMap["square hectometers"] = 10000.0;
     m_ratioMap["square centimeters"] = 0.0001;
     m_ratioMap["square milimeters"] = 0.000001;
     m_ratioMap["square stanjen (austriac)"] = 3.59665;
     m_ratioMap["hectares"] = 10000;
     m_ratioMap["jugar"] = 5755;
     m_ratioMap["acres"] = 4046.856422;
}
