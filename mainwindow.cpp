#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "basetabwidget.h"
#include "areaconv.h"
#include "lengthconvertor.h"
#include "angleconvertor.h"
#include "etrsstereo.h"
#include "stereoetrs.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->tabWidget->clear();
    ui->tabWidget->addTab(new EtrsStereo(ui->tabWidget),"ETRS89->STEREO70");
    ui->tabWidget->addTab(new StereoEtrs(ui->tabWidget), "STEREO70->ETRS89");
    ui->tabWidget->addTab(new BaseTabWidget(new AreaConv(),ui->tabWidget),"Area");
    ui->tabWidget->addTab(new BaseTabWidget(new LengthConvertor(),ui->tabWidget),"Length");
    ui->tabWidget->addTab(new BaseTabWidget(new AngleConvertor(),ui->tabWidget),"Angle");
}

MainWindow::~MainWindow()
{
    delete ui;
}
