QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    TransDAT/CommonClasses/BSplineInterpolation.cpp \
    TransDAT/CommonClasses/GRD.cpp \
    TransDAT/CommonClasses/HMNETRS89.cpp \
    TransDAT/CommonClasses/Helmert2D.cpp \
    TransDAT/CommonClasses/Interpolation_1D.cpp \
    TransDAT/CommonClasses/Interpolation_2D.cpp \
    TransDAT/ETRS89_STEREO70/ETRS89HMN.cpp \
    TransDAT/ETRS89_STEREO70/ETRS89Stereo30.cpp \
    TransDAT/ETRS89_STEREO70/ETRS89Stereo70.cpp \
    TransDAT/ETRS89_STEREO70/ETRS89Stereo70HMN.cpp \
    TransDAT/ETRS89_STEREO70/ETRS89_STEREO.cpp \
    TransDAT/ETRS89_STEREO70/Stereo_FiLa_NE.cpp \
    TransDAT/STEREO70_ETRS89/Stereo70ETRS89.cpp \
    TransDAT/STEREO70_ETRS89/Stereo70HMN_ETRS89.cpp \
    TransDAT/STEREO70_ETRS89/Stereo_NE_FiLa.cpp \
    angleconvertor.cpp \
    areaconv.cpp \
    basetabwidget.cpp \
    etrsstereo.cpp \
    lengthconvertor.cpp \
    main.cpp \
    mainwindow.cpp \
    baseconvertor.cpp \
    stereoetrs.cpp

HEADERS += \
    TransDAT/CommonClasses/BSplineInterpolation.h \
    TransDAT/CommonClasses/GRD.h \
    TransDAT/CommonClasses/HMNETRS89.h \
    TransDAT/CommonClasses/Helmert2D.h \
    TransDAT/CommonClasses/Interpolation_1D.h \
    TransDAT/CommonClasses/Interpolation_2D.h \
    TransDAT/CommonClasses/Position.h \
    TransDAT/CommonClasses/StereoNE.h \
    TransDAT/CommonClasses/TransDatResult.h \
    TransDAT/ETRS89_STEREO70/ETRS89HMN.h \
    TransDAT/ETRS89_STEREO70/ETRS89Stereo30.h \
    TransDAT/ETRS89_STEREO70/ETRS89Stereo70.h \
    TransDAT/ETRS89_STEREO70/ETRS89Stereo70HMN.h \
    TransDAT/ETRS89_STEREO70/ETRS89_STEREO.h \
    TransDAT/ETRS89_STEREO70/Stereo_FiLa_NE.h \
    TransDAT/STEREO70_ETRS89/Stereo70ETRS89.h \
    TransDAT/STEREO70_ETRS89/Stereo70HMN_ETRS89.h \
    TransDAT/STEREO70_ETRS89/Stereo_NE_FiLa.h \
    angleconvertor.h \
    areaconv.h \
    basetabwidget.h \
    etrsstereo.h \
    lengthconvertor.h \
    mainwindow.h \
    baseconvertor.h \
    stereoetrs.h

FORMS += \
    basetabwidget.ui \
    etrsstereo.ui \
    mainwindow.ui \
    stereoetrs.ui

TRANSLATIONS += \
    QtConvertor_ro_RO.ts

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    iconResources.qrc
